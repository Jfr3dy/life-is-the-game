﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
   public DanceController danceController;

   public void SetDance(int val)
   {
      danceController.SetAnimation(val);
   }
}
