﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Animator))]
public class DanceController : MonoBehaviour
{
    private Animator aninm;

    private void Awake()
    {
        aninm = GetComponent<Animator>();
    }

    public void SetAnimation(int val)
    {
        aninm.SetFloat("Dance", (float)val);
    }
}
