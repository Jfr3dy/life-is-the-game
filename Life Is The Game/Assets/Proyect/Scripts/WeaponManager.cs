﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public int indexWeapong;
    public GunBasic[] weapongs;
    public LayerMask mask;
    public Transform holderWeapong;

    private void Awake()
    {
        weapongs = new GunBasic[2];
        indexWeapong = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, .3f, transform.forward, out hit, 2.1f, mask))
            {
                if (hit.transform.GetComponent<GunBasic>() != null)
                {
                    SetWeapong(hit.transform.GetComponent<GunBasic>());
                }
            }
        }
    }

    void SetWeapong(GunBasic gun)
    {
        gun.transform.GetComponent<Rigidbody>().isKinematic = true;
        if (weapongs[indexWeapong] == null)
        {
            weapongs[indexWeapong] = gun;
        }
        else
        {
            //Soltar arma
        }
        gun.transform.parent = holderWeapong;
        gun.transform.localPosition = Vector3.zero;
        gun.transform.localRotation = Quaternion.identity;
    }

    void CahngeWeapong()
    {
        
    }
}
